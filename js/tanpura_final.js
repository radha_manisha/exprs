(function(window){
 var freqlist = [440,466,493,523,554,622,659,698,739,783,830,880];

 var isplaying = false;
 var freq=0;
 var freq_rand;
 var new_freq;
 var guess_freq = 0;
 var step_size_val_map = {
 //in Hz
 "0" : 0.5, // small step
 "5" : 10,  // medium step
 "10" : 20  // large step
 };
 var tone_playing = false;
 //var button1 = document.getElementById('btn1');
 var button2 = document.getElementById('btn2');

var sineosc = function(freq) {
  this.context = new webkitAudioContext();
  this.oscillator = this.context.createOscillator();
  this.tone_playing = false;
  
  return this;
 };

sineosc.prototype.play = function(freq) {
  this.oscillator.frequency.value = freq;
  this.oscillator.connect(this.context.destination);
  this.oscillator.noteOn(0);
  this.tone_playing = true;
};

sineosc.prototype.stop = function() {
  this.oscillator.disconnect();
  this.tone_playing = false;
};
sineosc.prototype.change = function(freq) {
  this.oscillator.frequency.value = freq;
};

var player = new sineosc();
var guesser = new sineosc();

 var buttonclick_result1 = function(){
	 var button= document.getElementById('btn1');
	 button.onclick = function buttonclick()
	 {
		 if(button.className == "off")
		 {
			 button.className="on";
			 button.src="ico-play.gif";
			 //alert("x");
			 play_tone();
		 }
		 else if(button.className == "on")
		 {
			 button.className="off";
			 button.src="ico-stop.gif";
			 //sineoscoff();
			player.stop();
		 }
	 }
 }

var buttonclick_result2 = function(){
	 var button1= document.getElementById('btn2');
	 button1.onclick = function buttonclick1()
	 {
		 if(button1.className == "off1")
		 {
			 button1.className="on1";
			 button1.src="ico-play.gif";
			 //alert("x");
			 guess_tone();
		 }
		 else if(button1.className == "on1")
		 {
			 button1.className="off1";
			 button1.src="ico-stop.gif";
			 guesser.stop();
			 //sineoscoff();
		 }
	 }
 }

 var on_play_click = function() {
	 if(tone_playing === true){
		 button1.src="ico-stop.gif";
		 player.stop();
	 }
	 else {
		 button1.src="ico-play.gif";
		 play_tone();
	 }
 }

 var on_guess_click = function() {
	 if(tone_playing){
		 guesser.stop();
		 button2.src="ico-stop.gif";
	 }
	 else{
		 guess_tone();
		 button2.src="ico-play.gif";
	 }
 }

 

 var play_tone = function() {
	 if(freq){
		 tone_playing=true;
	 }
	 else{
		 freq = freqlist[Math.floor(Math.random()*freqlist.length)];
	 }
	 player.play(freq);
 };

 var sineoscoff = function() {
 };

 var guess_tone = function() {
	if(guess_freq){
		tone_playing=true;
	}
	else {
	 guess_freq = Math.floor(Math.random()*((freq+50) - (freq-50)) + (freq-50));
	} 
	console.log(guess_freq);
	guesser.play(guess_freq);
 };

 var guess_up_clicked = function() {
	console.log('guess up clicked')
 var step_val = $('#step-size').val();
 var step = step_size_val_map[step_val];
	 guess_freq = guess_freq + step;
	 guesser.change(guess_freq);
	 var result = compare(guess_freq ,freq);
	 print_result(result);
 }

 var guess_down_clicked = function() {
	console.log('guess down clicked')
 var step_val = $('#step-size').val();
 var step = step_size_val_map[step_val];
	 guess_freq = guess_freq - step;
	 guesser.change(guess_freq);
	 var result = compare(guess_freq,freq);
 	 print_result(result);
 }

 var compare = function(guess_freq,freq) {
	console.log('compare()');
	var r = guess_freq - freq;
        console.log(r);
	console.log(guess_freq);
	console.log(freq);
	 var result =  Math.abs(r);
	 return result;
 }

var to_cents = function(result,freq) {
	return Math.floor( 1200 * Math.log( result / freq( note ))/Math.log(2) );
}
 var print_result = function(result) {
	 if ( result > 20 && result <=50) {
		 $('#demo').html('You are too far! Try again');
	 }else if (result >10 && result <=20) {
		 $('#demo').html('You are almost there !');
	 }else if (result <=10) {
		 $('#demo').html('Exact');
		 check();
	 }
 }

 var check = function() {
	player.stop();
	guesser.stop();
 }


 window.sineosc = sineosc;
 window.play_tone = play_tone;
 window.buttonclick_result1=buttonclick_result1;
 window.buttonclick_result2=buttonclick_result2;
 window.guess_tone = guess_tone;
 window.sineoscoff = sineoscoff;
 window.guess_up_clicked = guess_up_clicked;
 window.guess_down_clicked = guess_down_clicked;
 window.check = check;
})(window);

